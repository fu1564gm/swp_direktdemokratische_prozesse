import { defineStore } from 'pinia';
import axios from 'axios';

interface MiniVerfahren {
    id: number;
    title: string;
    phase: number;       // 0: 1. Phase, 1: 2. Phase, 2: 3. Phase
    success: number;     // 0: laufend, 1: erfolgreich, 2: gescheitert
    description: string;
}

interface DetailVerfahren {
    id: number;
    title: string;
    phase: number;       // 0: 1. Phase, 1: 2. Phase, 2: 3. Phase
    success: number;     // 0: laufend, 1: erfolgreich, 2: gescheitert
    carrier: string;
    created: Date;
    end: Date;
    description: string;
    // Social Media Links
}

interface User { // Was muss gespeichert werden und was kann man vom Token ableiten?
    id: number;
    name: string;
    email: string;
    token: string; // oder doch einzeln?
}

export const useStore = defineStore('store', {
    state: () => ({
        currentUser: null as User | null,
        token: '', // nur einzeln, wenn nicht in User
        verfahrenList: [] as MiniVerfahren[],
        featuredVerfahren: [] as MiniVerfahren[],
        chosenVerfahren: {
            id: 0,
            title: 'Test-Titel',
            phase: 0,
            success: 0,
            carrier: 'Test-Traeger',
            created: new Date(),
            end: new Date(),
            description: 'Test-Beschreibung'
        } as DetailVerfahren, 
        filterOptions: [] as string[], // TODO: Standard festlegen
        sortOption: '0', // TODO: Standard festlegen
        filterVisible: false, // Such-/Filter-Menü Sichtbarkeit
        abgeschlossen: false, // Listentyp
        listLimit: 5,
        maxListLength: 0,
        sortMethod(a : MiniVerfahren, b : MiniVerfahren, sortOption : string) {
            switch (parseInt(sortOption)) {
                case 0: // alphabetisch aufsteigend
                    if (a.title.toLowerCase() < b.title.toLowerCase()) {
                        return -1;
                    }
                    if (a.title.toLowerCase() > b.title.toLowerCase()) {
                        return 1;
                    }
                    return 0;
                case 1: // alphabetisch absteigend
                    if (b.title.toLowerCase() < a.title.toLowerCase()) {
                        return -1;
                    }
                    if (b.title.toLowerCase() > a.title.toLowerCase()) {
                        return 1;
                    }
                    return 0;
                case 4: // Phase aufsteigend
                    return a.phase - b.phase;
                case 5: // Phase absteigend
                    return b.phase - a.phase;
                default:
                    console.error('Unknown sortOption in VerfahrenList', sortOption, typeof(sortOption));
                    return 0;
            }
        },
        scrollToId(id : string) {
            const elem = document.getElementById(id);
            if (elem) {
                if (elem.style.display != "none") {
                    const headerOffset = 133;
                    const elemPos = elem.getBoundingClientRect().top;
                    const offsetPos = elemPos + window.pageYOffset - headerOffset;
                    window.scrollTo({
                        top: offsetPos,
                        behavior: "smooth",
                    });  
                }  
            }
        },
    }),
    actions: {
        getBackendData() { // TODO: Mit Backend verbinden!
            axios.get('db.json').then((response) => {
                this.sortAndFilter(response.data as MiniVerfahren[]);
            })
        },
        sortAndFilter(data : MiniVerfahren[]) { // TODO: Weitere Filter-/Sortieroptionen hinzufügen
            let verfahrenList = [] as MiniVerfahren[];
            if (!this.abgeschlossen) {
                verfahrenList = data.filter(elem => !elem.success).sort((a,b) =>this.sortMethod(a, b, this.sortOption));
            }
            else {
                verfahrenList = data.filter(elem => elem.success).sort((a,b) => this.sortMethod(a, b, this.sortOption));
                if (this.filterOptions.length > 0 && (this.filterOptions.includes('3') || this.filterOptions.includes('4'))) {
                    verfahrenList = verfahrenList.filter(elem => this.filterOptions.includes((elem.success+2).toString()));
                }
            }
            if (this.filterOptions.length > 0 && (this.filterOptions.includes('0') || this.filterOptions.includes('1') || this.filterOptions.includes('2'))) {
                verfahrenList = verfahrenList.filter(elem => this.filterOptions.includes((elem.phase).toString()));
            }
            this.maxListLength = verfahrenList.length;
            if (this.listLimit < this.maxListLength) {
                verfahrenList = verfahrenList.slice(0, this.listLimit-1);
            }
            this.verfahrenList = verfahrenList;
        },
        setFeatured() { // TODO: Backendverbindung + aktuellste zuerst
            axios.get('db.json').then((response) => {
                this.featuredVerfahren = (response.data as MiniVerfahren[]).filter(elem => !elem.success).sort((a,b) => this.sortMethod(a, b, '0'));
            });
        },
        setChosenVerfahren(id : number) {
            /* TODO: Datenbank über id anfragen, um Details zu erhalten; bis jetzt nur Platzhalter */
            console.log('setChosenVerfahren:', id);
            const beispiel = {
                id: 0, 
                title: "Deutsche Wohnen enteignen", 
                phase: 0, 
                success: 0,
                carrier: "Heinz Maier",
                created: new Date("2015-01-10"),
                end: new Date("2019-01-16"),
                description: "Unsere Initiative „Deutsche Wohnen & Co enteignen“ wird von engagierten Einzelpersonen sowie Menschen aus Mieter:inneninitiativen, stadtpolitischen Gruppen und Parteien getragen. Wir haben ganz unterschiedliche Hintergründe.",
            };
            this.chosenVerfahren = beispiel;
        },

    }
});