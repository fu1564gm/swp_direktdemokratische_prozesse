import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '@/views/MainView.vue'
import NotFound from '@/views/NotFound.vue'
import AboutView from '@/views/AboutView.vue'
import RegisterLoginView from '../views/RegisterLoginView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFound
  },
  {
    path: '/about',
    name: 'about',
    component: AboutView
  },
  {
    path: '/login',
    name: 'login',
    component: RegisterLoginView,
    props: { registered: true }
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterLoginView,
    props: { registered: false }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
