# swp_direktdemokratische_prozesse

## Sinnvolle VS Code Erweiterungen

- Vue Language Features (Volar), von Vue
- Vue Language Features (Volar), von Vue
- ESLint, von Microsoft


## Erweiterungen gegenüber einem Barebone VueJS Projekt

- [Babel](https://babeljs.io/docs/en/), Service welcher die JS Anwendung backwards kompatibel macht
- [Router](https://router.vuejs.org/guide/), Seitenabhängigkeit bzw. Link Logik regeln
- [ES-Lint](https://eslint.vuejs.org/), checkt Code Formatierung und bestimmte Coding Conventions
- [Unittesting](https://vuejs.org/guide/scaling-up/testing.html), Testet Codelogik, Tests werden in Gitlab nach dem Commit gecheckt
- [Typescript](https://vuejs.org/guide/typescript/overview.html), ermöglicht Logik in Code auszudrücken, regelt Code/Klasen Formate, ...


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
